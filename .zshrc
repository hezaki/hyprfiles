[ -f "$HOME/.local/share/zap/zap.zsh" ] && source "$HOME/.local/share/zap/zap.zsh"
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

export EDITOR='nvim'
export BAT_THEME="Nord"
export FZF_DEFAULT_OPTS=$FZF_DEFAULT_OPTS'
    --color=fg:#e5e9f0,bg:#2e3440,hl:#81a1c1
    --color=fg+:#e5e9f0,bg+:#2e3440,hl+:#81a1c1
    --color=info:#eacb8a,prompt:#bf6069,pointer:#b48dac
    --color=marker:#a3be8b,spinner:#b48dac,header:#a3be8b'

_fix_cursor() {
   echo -ne "\033[4 q"
}
precmd_functions+=(_fix_cursor)
alias notify-send="notify-send.sh"

plug "MichaelAquilina/zsh-auto-notify"
plug "zsh-users/zsh-history-substring-search"
plug "zdharma-continuum/fast-syntax-highlighting"
plug "zsh-users/zsh-autosuggestions"
plug "romkatv/powerlevel10k"
plug "zap-zsh/supercharge"

export AUTO_NOTIFY_THRESHOLD=20
export AUTO_NOTIFY_TITLE="%command finished"

alias :q!="doas poweroff"
alias :x!="doas reboot"
alias cat="bat"
alias vi="nvim -p"
alias :wq="exit"
alias :q="exit"
alias py="python"
alias tree="ls --tree"
alias l="ls -l"
alias la="ls -la"
alias ll="ls -ll"
alias ..='cd ..'
alias md="mkdir"
alias ls="lsd -F"

xf ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   tar xf $1    ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

bindkey "^[[1;5C" forward-word
bindkey "^[[1;5D" backward-word

[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
