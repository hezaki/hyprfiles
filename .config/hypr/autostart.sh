#!/usr/bin/bash

hyprpaper &
waybar -c $HOME/.config/hypr/waybar/config.jsonc -s $HOME/.config/hypr/waybar/style.css & 
dunst &
hyprctl setcursor Radioac 1 &
gsettings set org.gnome.desktop.interface gtk-theme ndsb &
gsettings set org.gnome.desktop.interface cursor-theme Radioac &
QT_QPA_PLATFORM=wayland
