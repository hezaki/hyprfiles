
<h1 align="center">Dotfiles</h1>

![Screenshot](/img/1.png)
![Screenshot](/img/2.png)

# Details
- **OS**: [Artix Linux](https://artixlinux.org/)
- **Init**: [Dinit](https://github.com/davmac314/dinit)
- **WM**: [Hyprland](https://github.com/hyprwm/Hyprland)
- **Bar**: [Waybar](https://github.com/Alexays/Waybar)
- **Notifications**: [Dunst](https://github.com/dunst-project/dunst)
- **Wallpaper**: `/img/wallpaper.png`
- **Terminal**: [Foot](https://codeberg.org/dnkl/foot)
- **Plugins manager**: [zap-zsh](https://www.zapzsh.org/)
- **Launcher** [Rofi](https://github.com/davatorium/rofi)
- **Browser**: [LibreWolf](https://librewolf.net/)
- **Editor**: [Neovim](https://github.com/neovim/neovim)
- **Font**: [JetBrainsMono](https://github.com/JetBrains/JetBrainsMono)
- **Pallete**: [Nord](https://www.nordtheme.com/)
- **GTK Theme**: [Nordic](https://www.pling.com/p/1267246)
- **Cursor Theme**: [Radioactive-Nord](https://www.pling.com/p/1677966) 

# Tutorial
```bash
git clone https://codeberg.org/hezaki/nordfiles.git
cd nordfiles
cp -r .config ~/
cp -r chrome ~/.librewolf/user/    # or any other firefox-based browser
cp -r .zshrc ~/
```
# Packages (Arch)
```shell
hyprland waybar-hyprland hyprpaper foot swaylock-effects rofi-lbonn-wayland dunst grim slurp ranger tmux neovim zathura zsh bat lsd
```
# Keybinds
- `SUPER+D`: open rofi
- `SUPER+B`: hide/show waybar
- `SUPER+1,...9`: Switch workspace
- `SHIFT+SUPER+1,...9,0`: Move window to workspace
- `PRINT`: Screenshot
- `SUPER+PRINT`: Highlight a screenshot
- `SUPER+ENTER`: Open terminal (wow)
- `SUPER+F`: fullscreen
- `SUPER+S`: floating/tiling
- `SUPER+W`: close
- `F11, F12, F10`: Manipulating the sound

